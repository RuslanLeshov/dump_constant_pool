package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;

public class RefParser extends ConstantParser {
    @Override
    public Constant parseData(int tag, int index, ByteBuffer buffer) {
        short classIndex = buffer.getShort();
        short nameAndTypeIndex = buffer.getShort();

        String type;
        switch (tag) {
            case 9:
                type = "Fieldref";
                break;
            case 10:
                type = "Methodref";
                break;
            case 11:
                type = "InterfaceMethodref";
                break;
            default:
                throw new RuntimeException("Wrong parser chosen for tag " + tag);
        }
        return format(index, type, String.format("#%d.#%d", classIndex, nameAndTypeIndex));
    }
}
