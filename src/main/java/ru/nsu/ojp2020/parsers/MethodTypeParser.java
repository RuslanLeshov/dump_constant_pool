package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;

public class MethodTypeParser extends ConstantParser {
    @Override
    public Constant parseData(int tag, int index, ByteBuffer buffer) {
        short descriptorIndex = buffer.getShort();
        return format(index, "MethodType", "#" + descriptorIndex);
    }
}
