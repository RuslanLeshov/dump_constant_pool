package ru.nsu.ojp2020;

import ru.nsu.ojp2020.parsers.ConstantParser;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: dump_constant_pool filename.class");
            return;
        }
        String filename = args[0];
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(filename));
            ByteBuffer buffer = ByteBuffer.wrap(bytes, 6, bytes.length - 6);

            short majorVersion = buffer.getShort();
            System.out.println("Class file format version: " + majorVersion);
            System.out.println();

            short constantPoolSize = buffer.getShort();

            for (int i = 0; i < constantPoolSize - 1; i++) {
                byte tag = buffer.get();
                ConstantParser parser = ConstantParser.forTag(tag);
                parser.parseData(tag, i + 1, buffer);
            }

            ConstantParser.getConstantPool().values().forEach(v -> System.out.println(v.formatToString(ConstantParser.getConstantPool())));


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
