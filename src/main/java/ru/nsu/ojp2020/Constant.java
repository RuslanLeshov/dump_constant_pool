package ru.nsu.ojp2020;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constant {
    private final int index;
    private final String type;
    private final String value;

    public Constant(int index, String type, String value) {
        this.index = index;
        this.type = type;
        this.value = value;
    }

    public String formatToString(Map<Integer, Constant> constantPool) {
        String normalizedString = getNormalizedString(constantPool);
        if (!normalizedString.equals(value)) {
            return String.format("%4s =\t%15s\t%30s\t%-50s", "#" + index, type, value, "// " + normalizedString);
        }
        return String.format("%4s =\t%15s\t%30s", "#" + index, type, value);
    }

    public String getNormalizedString(Map<Integer, Constant> constantPool) {
        String ret = value;
        Pattern pattern = Pattern.compile("#[0-9]+");
        Matcher matcher = pattern.matcher(value);
        if (type.equals("Invoke") || type.equals("InvokeDynamic")) {
            matcher.find(); // skip one
            matcher.group();
        }
        while (matcher.find()) {
            String ref = matcher.group();
            int key = Integer.parseInt(ref.substring(1));
            String val = constantPool.get(key).getNormalizedString(constantPool);
            ret = ret.replaceFirst(ref, Matcher.quoteReplacement(val));
        }
        return ret;
    }
}
