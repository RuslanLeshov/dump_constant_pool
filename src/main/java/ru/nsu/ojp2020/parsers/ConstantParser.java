package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public abstract class ConstantParser {
    private static final Map<Integer, Constant> constantPool = new HashMap<>();
    public static ConstantParser forTag(byte tag) {
        switch (tag) {
            case 1: return new Utf8Parser();
            case 3:
            case 4: return new IntegerFloatParser();
            case 5:
            case 6: return new LongDoubleParser();
            case 7: return new ClassParser();
            case 8: return new StringParser();
            case 9:
            case 10:
            case 11: return new RefParser();
            case 12: return new NameAndTypeParser();
            case 15: return new MethodHandleParser();
            case 16: return new MethodTypeParser();
            case 17:
            case 18: return new DynamicParser();
            case 19: return new ModuleParser();
            case 20: return new PackageParser();
            default:
                throw new  RuntimeException("unknown tag " + tag);
        }
    }

    protected Constant format(Integer index, String type, String value) {
        Constant constant = new Constant(index, type, value);
        constantPool.put(index, constant);
        return constant;
    }

    public abstract Constant parseData(int tag, int index, ByteBuffer buffer);

    public static Map<Integer, Constant> getConstantPool() {
        return constantPool;
    }
}
