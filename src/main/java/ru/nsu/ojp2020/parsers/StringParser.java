package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;

public class StringParser extends ConstantParser {
    @Override
    public Constant parseData(int tag, int index, ByteBuffer buffer) {
        short stringIndex = buffer.getShort();

        return format(index, "String", "#" + stringIndex);
    }
}
