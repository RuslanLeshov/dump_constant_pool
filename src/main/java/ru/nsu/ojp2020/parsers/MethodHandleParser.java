package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;

public class MethodHandleParser extends ConstantParser {
    @Override
    public Constant parseData(int tag, int index, ByteBuffer buffer) {
        byte referenceKind = buffer.get();
        short referenceIndex = buffer.getShort();

        return format(index, "MethodHandle", String.format("%d:#%d", referenceKind, referenceIndex));
    }
}
