package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;

public class IntegerFloatParser extends ConstantParser {
    @Override
    public Constant parseData(int tag, int index, ByteBuffer buffer) {
        String value;
        String type;
        switch (tag) {
            case 3:
                type = "Integer";
                value = Integer.toString(buffer.getInt());
                break;
            case 4:
                type = "Float";
                value = Float.toString(buffer.getFloat());
                break;
            default:
                throw new RuntimeException("Wrong parser chosen for tag " + tag);
        }
        return format(index, type, value);
    }
}
