package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;

public class DynamicParser extends ConstantParser {
    @Override
    public Constant parseData(int tag, int index, ByteBuffer buffer) {
        short bootstrapMethodAttrIndex = buffer.getShort();
        short nameAndTypeIndex = buffer.getShort();
        String type;
        switch (tag) {
            case 17:
                type = "Dynamic";
                break;
            case 18:
                type = "InvokeDynamic";
                break;
            default:
                throw new RuntimeException("Wrong parser chosen for tag " + tag);
        }
        return format(index, type, String.format("#%d:#%d", bootstrapMethodAttrIndex, nameAndTypeIndex));
    }
}
