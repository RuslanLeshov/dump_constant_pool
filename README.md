# dump_constant_pool

Программа для вывода пула констант из .class файла

Для запуска необходимо иметь установленную Maven  
Сборка: `mvn clean install`  
Запуск: `java -jar target/dump_constant_pool-1.0.jar filename.class`  
где filename это путь до класс-файла, либо просто имя файла в случае когда он находится в текущей директории