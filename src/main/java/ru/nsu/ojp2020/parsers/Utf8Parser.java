package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class Utf8Parser extends ConstantParser {
    @Override
    public Constant parseData(int tag, int index, ByteBuffer buffer) {
        short length = buffer.getShort();
        byte[] valueBytes = new byte[length];
        buffer.get(valueBytes, 0, length);
        String value = new String(valueBytes, StandardCharsets.UTF_8);

        return format(index, "Utf8", value);
    }
}
