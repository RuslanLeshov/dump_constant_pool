package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;

public class LongDoubleParser extends ConstantParser {
    @Override
    public Constant parseData(int tag, int index, ByteBuffer buffer) {
        String value;
        String type;
        switch (tag) {
            case 5:
                type = "Long";
                value = Long.toString(buffer.getLong());
                break;
            case 6:
                type = "Double";
                value = Double.toString(buffer.getDouble());
                break;
            default:
                throw new RuntimeException("Wrong parser chosen for tag " + tag);
        }
        return format(index, type, value);
    }
}
