package ru.nsu.ojp2020.parsers;

import ru.nsu.ojp2020.Constant;

import java.nio.ByteBuffer;

public class NameAndTypeParser extends ConstantParser {
    @Override
    public Constant parseData(int tag, int index, ByteBuffer buffer) {
        short nameIndex = buffer.getShort();
        short descriptorIndex = buffer.getShort();

        return format(index, "NameAndType", String.format("#%d:#%d", nameIndex, descriptorIndex));
    }
}
